
/*Ispis naziva županije */

/* 1 Prikaz imena na određenom mjestu*/

/* Ide u svg - to je lokacija na kojoj će se ispisivati tekst*/
<text class="proba" id="bla" x="0" y="20"  visibility="visible" dominant-baseline="hanging" >blaaaaaaaaaa</text>
 /* Ide u g/path poligona županije*/ 
 onmouseover="displayName('Krapinsko-zagorska županija')"
/* js na dnu svg*/
function displayName(name) {
    document.getElementById('bla').firstChild.data = name;
}





/* 2 Prikaz teksta na mousemove i mouse out*/
<text class="proba" id="bla" x="0" y="20"  visibility="visible" dominant-baseline="hanging" >blaaaaaaaaaa</text>

<script type="text/ecmascript"><![CDATA[
    (function() {
        var tooltip = document.getElementById('bla');
        var triggers = document.getElementsByClassName('pol');

        for (var i = 0; i < triggers.length; i++) {
            triggers[i].addEventListener('mousemove', showTooltip);
            triggers[i].addEventListener('mouseout', hideTooltip);
        }

        function showTooltip(evt) {
            tooltip.setAttributeNS(null, "visibility", "visible");
        }

        function hideTooltip(evt) {
            tooltip.setAttributeNS(null, "visibility", "hidden");
        }
    })()
]]></script>




/* 3 Prikaz naziva županija bez pozadine */
<text id="bla" x="0" y="0" visibility="hidden" dominant-baseline="hanging">hhhhhhh</text>


<script type="text/javascript"><![CDATA[

    (function() {
        var svg = document.getElementById("karta");
        var tooltip = svg.getElementById("bla");
        var triggers = svg.getElementsByClassName("pol");
        

        for (var i = 0; i < triggers.length; i++) {
            triggers[i].addEventListener('mousemove', showTooltip);
            triggers[i].addEventListener('mouseout', hideTooltip);
        }

        function showTooltip(evt) {
				var CTM = svg.getScreenCTM();
				var mouseX = (evt.clientX - CTM.e) / CTM.a;
				var mouseY = (evt.clientY - CTM.f) / CTM.d;
				tooltip.setAttributeNS(null, "x", mouseX + 6 / CTM.a);
				tooltip.setAttributeNS(null, "y", mouseY + 20 / CTM.d);
				tooltip.setAttributeNS(null, "visibility", "visible");
        tooltip.firstChild.data = evt.target.parentNode.getAttributeNS(null, "id");
        
			}

        function hideTooltip(evt) {
            tooltip.setAttributeNS(null, "visibility", "hidden");
        }

    })()
]]></script>


/* 4 Prikaz naziva županija sa pozadinom */
<g id="poligon" visibility="visible">
    <rect width="80" height="20" fill="white"/>
    <text x="4" y="15" dominant-baseline="centered">Tooltip</text>
</g>


<script type="text/ecmascript"><![CDATA[
    (function() {
        var pol = document.getElementById("poligon");
        var te = pol.getElementsByTagName("text")[0].firstChild;
        var polRe = pol.getElementsByTagName("rect");
        var svg = document.getElementById("karta");
        var pro = document.getElementById("bla");
                       
        
        var land = document.getElementsByClassName("pol");
        for (var i = 0; i < land.length; i++) {
            land[i].addEventListener('mousemove', showTooltip);
            land[i].addEventListener('mouseout', hideTooltip);
            
        }

    function showTooltip(evt) {
        
      var CTM = svg.getScreenCTM();
        var mouseX = (evt.clientX - CTM.e + 1) / CTM.a;
      var mouseY = (evt.clientY - CTM.f + 1) / CTM.d;
        pol.setAttributeNS(null, "transform", "translate(" + mouseX + " " + mouseY + ")");
      pol.setAttributeNS(null, "visibility", "visible");
      te.data = evt.target.parentNode.getAttributeNS(null, "id");

        var length = te.parentNode.getComputedTextLength();
        for (var i = 0; i < polRe.length; i++) {
            polRe[i].setAttributeNS(null, "width", length + 8);
        }
       }
        
    
    function hideTooltip(evt) {
            pol.setAttributeNS(null, "visibility", "hidden");
        }

    })()
]]>
</script> 
